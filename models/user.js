const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
	username: {
		type 	: String,
		index	: true
	},
	password:	{
		type 	: String, 
		required: true, 
		bcrypt  : true
	},
	email: String,
	name: String,
	surname: String
});

module.exports =  mongoose.model('User', UserSchema);
