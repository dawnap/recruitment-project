const User = require('../models/user');
const bcrypt = require('bcrypt');
const passport = require('passport');
const localStrategy = require('passport-local').Strategy;

/*
    REJESTRACJA
*/

const register = (req, res, next) => {
    let name 		      	=	 req.body.name;
	let surname 		    =	 req.body.surname;
	let email 			    =	 req.body.email;
	let username 		    =	 req.body.username;
	let password 		    =	 req.body.password;
    let confirmPassword	    =	 req.body.confirmPassword;

	req.checkBody('name', 'Imię jest wymagane').notEmpty();
	req.checkBody('surname', 'Nazwisko jest wymagane').notEmpty();
	req.checkBody('email', 'Email jest wymagany').notEmpty();
	req.checkBody('email', 'Email jest niepoprawny').isEmail();
	req.checkBody('username', 'Nazwa użytkownika jest wymagana').notEmpty();
	req.checkBody('password', 'Hasło jest wymagane').notEmpty();
	req.checkBody('confirmPassword', 'Hasła się nie zgadzają').equals(req.body.password);

	// Błędy walidacji
	var errors = req.validationErrors();

	if (errors) {
		res.render('register', {
			req						:	req,
			title					:	'Rejestracja',
			errors 					: 	errors,
			name 					: 	name,
			surname 				: 	surname,
			email 					: 	email,
			username 				: 	username,
			password 				: 	password,
			confirmPassword         : 	confirmPassword
		});
	} else {
		// Tworzenie modelu użytkownika
		var newUser	= new User({
			name 		: 	name,
			surname 	: 	surname,
			email 		: 	email,
			username 	: 	username,
			password 	: 	password
		});

		console.log(newUser);
		
		var salt = 10;

		bcrypt.hash(newUser.password, salt, (err, hash) => {
			if (err) throw err;

			// Ustawianie hasha jako hasło
			newUser.password = hash;

			// Tworzenie nowego uzytkownika
			newUser.save(newUser, (err, user) => {
				if (err)  throw err;
				console.log(user);
			});

			req.flash('success', 'Utworzono konto');

			res.location('/');
			res.redirect('/');
			});
	}
};



/*
    LOGOWANIE
*/

// Czwarta faza logowania - tworzenie sesji użytkownika przez serializowanie
passport.serializeUser((user, done) => {
    done(null, user[0].id);
});

passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
        done(err, user);
    });
});


// Trzecia faza logowania - porównanie haseł
const comparePassword = (candidatePassword, hash, callback) => {
    bcrypt.compare(candidatePassword, hash, (err, matched) => {
        if (err) return callback(err);
        callback(null, matched);
    });
};


// Druga faza logowania
passport.use(new localStrategy((username, password, done) => {
    User.find({username : username}, (err, user) => {
        
        if (err) throw err;
        if (user.length == 0) {
            console.log('Unknown User');
            return done(null, false, {message: 'Unknown User'});
        } 
        
        comparePassword(password, user[0].password, (err, matched) => {
            if (err) throw err;
            if (matched) {
                return done(null, user);
            } else {
                console.log('Invalid Password');
                return done(null, false, {message: 'Invalid Password'});
            }
        });
    });
}));

// Po zalogowaniu
const afterLogin = (req, res) => {
    console.log('Authentication Successful');
	req.flash('success', 'Zalogowano');
	res.redirect('/');
}

// Wylogowanie
const logout = (req, res) => {
    req.logout();
    req.flash('success', 'Wylogowano');
    res.redirect('/logowanie');
}

// Czy zalogowany
const ensureAuthenticated = (req, res, next) => {
	if (req.isAuthenticated()) {
		return next();
	}
	req.flash('error', 'Musisz się zalogować.');
	res.redirect('/logowanie');
}

// Czy niezalogowany
const ensureNotAuthenticated = (req, res, next) => {
	if (!req.isAuthenticated()) {
		return next();
	}
	req.flash('error', 'Jesteś zalogowany.');
	res.redirect('/');
}

module.exports = {
    register                : register,
    afterLogin              : afterLogin,
    logout                  : logout,
    ensureAuthenticated     : ensureAuthenticated,
    ensureNotAuthenticated  : ensureNotAuthenticated
};