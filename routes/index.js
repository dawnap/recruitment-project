const express = require('express');
const router = express.Router();

const User 					= require('../models/user');
const bcrypt 	    	= require('bcrypt');
const passport			= require('passport');
const localStrategy =	require('passport-local').Strategy;

const userController = require('../controllers/user');
const contactController = require('../controllers/contact');

const ensureAuthenticated = userController.ensureAuthenticated;
const ensureNotAuthenticated = userController.ensureNotAuthenticated;

router.get('/', ensureAuthenticated, async (req, res, next) => {
	let contacts = await contactController.get(req, res, next);
	console.log(contacts);
  res.render('index', {
		contacts: contacts,
		req: req,
		authenticated: req.isAuthenticated(),
		title: 'Strona główna'
	});
});

router.get('/contact/add', ensureAuthenticated, (req, res, next) => {
	res.render('contact/add', {
		req: req,
		authenticated: req.isAuthenticated(),
		title: 'Dodawanie kontaktu'
	});
});

router.post('/contact/add', ensureAuthenticated, async (req, res, next) => {
	let contact = await contactController.add(req, res, next);
	if (contact) {
		res.redirect(`/contact/view/${contact.id}/?success=added`);
	} else {
		res.redirect(`/contact/view/${contact.id}/?error=couldNotAdd`);
	}
});

router.get('/contact/edit/:contactId', ensureAuthenticated, async (req, res, next) => {
	let contact = await contactController.getOne(req, res, next);
	res.render('contact/edit', {
		contact: contact,
		req: req,
		authenticated: req.isAuthenticated(),
		title: 'Edytowanie kontaktu'
	});
});

router.post('/contact/edit/:contactId', ensureAuthenticated, async (req, res, next) => {
	let contact = await contactController.update(req, res, next);
	if (contact) {
		res.redirect(`/contact/view/${req.params.contactId}/?success=updated`);
	} else {
		res.redirect(`/contact/view/${req.params.contactId}/?error=couldNotUpdate`);
	}
});

router.get('/contact/delete/:contactId', ensureAuthenticated, async (req, res, next) => {
	let contact = await contactController.deleteOne(req, res, next);
	if (contact) {
		res.redirect(`/?success=deleted&id=${req.params.contactId}`);
	} else {
		res.redirect(`/?error=couldNotDelete&id=${req.params.contactId}`);
	}
});

router.get('/contact/view/:contactId', ensureAuthenticated, async (req, res, next) => {
	let contact = await contactController.getOne(req, res, next);
	res.render('contact/view', {
		contact: contact,
		req: req,
		authenticated: req.isAuthenticated(),
		title: 'Kontakt'
	});
});

router.get('/rejestracja', ensureNotAuthenticated, (req, res, next) => {
  res.render('register', {
		req: req,
    title: 'Rejestracja'
  });
});

router.get('/logowanie', ensureNotAuthenticated, (req, res, next) => {
  res.render('login', {
		req: req,
    title: 'Logowanie'
  });
});

// Obsługa zapytania POST z formularza rejestracji
router.post('/rejestracja', ensureNotAuthenticated, (req, res, next) => userController.register(req, res, next));

// Pierwsza faza logowania - odbiór danych z zapytania POST
router.post('/logowanie', ensureNotAuthenticated, passport.authenticate('local', {failureRedirect: '/logowanie', failureFlash: 'Invalid Username or Password'}), (req, res) => {
	userController.afterLogin(req, res);
});

// Wylogowywanie
router.get('/wyloguj', ensureAuthenticated, (req, res) => {
  userController.logout(req, res);
});

module.exports = router;
