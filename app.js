const express = 	require('express');
const path = 	require('path');
const favicon = 	require('serve-favicon');
const logger = 	require('morgan');
const expressValidator = 	require('express-validator');
const cookieParser = 	require('cookie-parser');
const bodyParser = 	require('body-parser');
const session = 	require('express-session');
const passport = 	require('passport');
const multer = 	require('multer');
const flash =	require('connect-flash');
const mongoose = require('mongoose');
const MongoStore  = require('connect-mongo')(session);

const routes = 	require('./routes/index');

const app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');


app.use(multer({dest:__dirname+'/uploads/'}).any());


//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use(session({
	secret:'secret',
	saveUninitialized: true,
	resave: true,
	store: new MongoStore({
		mongooseConnection: mongoose.connection
	})
}));


app.use(passport.initialize());
app.use(passport.session());


app.use(expressValidator({
	errorFormator: function(param,msg,value) {
    var namespace = param.split('.'),
      root = namespace.shift(),
      formParam = root;
		while(namespace.length) {
			formParam += '[' + namespace.shift() + ']';
		}

		return {
			param 	: formParam,
			msg 	: msg,
			value	: value
		};
	}
}));


app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(flash());
app.use(function(req,res,next) {
	res.locals.messages = require('express-messages')(req,res);
	next();
});


app.get('*', function(req,res,next) {
	res.locals.user = req.user ||  null;
	next();
});

app.use('/', routes);


app.use((req, res, next) => {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
