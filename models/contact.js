const mongoose = require('mongoose');

const ContactSchema = mongoose.Schema({
	ownerId: String,
	name: {
        first: {
			type    : String,
			index	: true
        },
		second: {
            type    : String,
			index	: true
		},
		nickname: {
			type    : String,
			index	: true
		}
	},
	numbers: [{
		num: {
			type: String
		},
		type: {
			type: String
		}
	}],
	emails: [{
		email: {
			type: String
		}
	}],
	address: {
		address1: {
			type: String
		},
		address2: {
			type: String
		},
		postcode: {
			type: String
		},
		city: {
			type: String
		}
	},
	gravatar: {
		type: String
	},
	job: {
		position: {
			type: String
		},
		company: {
			type: String
		}
	}
});

module.exports =  mongoose.model('Contact', ContactSchema);
