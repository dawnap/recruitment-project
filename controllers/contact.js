const Contact = require('../models/contact');
const User = require('../models/user');

const md5 = require('md5');

const add = async (req, res, next) => {
    if (req.user) {

        const contact = req.body.add;

        let data = {
            ownerId: req.user._id,
            ...contact,
            gravatar: (contact.emails && [...contact.emails].shift() && [...contact.emails].shift().email) ? md5([...contact.emails].shift().email) : null,
        };

        data.numbers.forEach((el, i, a) => {
            if (data.numbers[i].num == '')  delete data.numbers[i];
            if (data.numbers[i].type == '') delete data.numbers[i].type;
        });
        data.emails.forEach((el, i, a) => {
            if (data.emails[i].email == '') delete data.emails[i];
        });

        if (data.job.position == '')        delete data.job.position;
        if (data.job.company == '')         delete data.job.company;


        if (data.address.address1 == '')    delete data.address.address1;
        if (data.address.address2 == '')    delete data.address.address2;
        if (data.address.postcode == '')    delete data.address.postcode;
        if (data.address.city == '')        delete data.address.city;

        if (data.name.first == '')          delete data.name.first;
        if (data.name.second == '')         delete data.name.second;
        if (data.name.nickname == '')       delete data.name.nickname;

        let newContact = new Contact(data);

        let returnValue;

        await newContact.save().then((result) => {
            returnValue = result;
        }).catch(e => {
            console.error(e);
            return false;
        });

        return returnValue;

    } else {
        console.warn('Contact -> add() bez logowania');
        return false;
    }
};

const update = async (req, res, next) => {

    let returnValue;

    if (req.user) {
        
        const contact = req.body.add;

        let data = {
            ownerId: req.user._id,
            ...contact,
            gravatar: (contact.emails && [...contact.emails].shift() && [...contact.emails].shift().email) ? md5([...contact.emails].shift().email) : null,
        };

        data.numbers.forEach((el, i, a) => {
            if (data.numbers[i]) {
                if (data.numbers[i].num && data.numbers[i].num == '')  delete data.numbers[i];
                if (data.numbers[i].type && data.numbers[i].type == '') delete data.numbers[i].type;
            }
        });
        data.emails.forEach((el, i, a) => {
            if (data.emails[i].email == '') delete data.emails[i];
        });

        if (data.job.position == '')        delete data.job.position;
        if (data.job.company == '')         delete data.job.company;


        if (data.address.address1 == '')    delete data.address.address1;
        if (data.address.address2 == '')    delete data.address.address2;
        if (data.address.postcode == '')    delete data.address.postcode;
        if (data.address.city == '')        delete data.address.city;

        if (data.name.first == '')          delete data.name.first;
        if (data.name.second == '')         delete data.name.second;
        if (data.name.nickname == '')       delete data.name.nickname;

        await Contact.updateOne({ _id: req.params.contactId }, data).then(result => {
            returnValue = result;
        }).catch(e => {
            console.log(e);
            return false;
        });
        return returnValue;
    } else {
        console.warn('Contact -> update() bez logowania');
        return false;
    }
};

const deleteOne = async (req, res, next) => {
    let returnValue;
    if (req.user) {
        await Contact.deleteOne({ _id: req.params.contactId }).then(result => {
            returnValue = result;
        }).catch(e => {
            console.error(e);
            return false;
        });
        return returnValue;
    } else {
        console.warn('Contact -> deleteOne() bez logowania');
        return false;
    }
}

const get = async (req, res, next) => {

    let contacts;

    if (req.user) {

        await Contact.find({ ownerId: req.user._id }).then(result => {
            contacts = result;
        }).catch(e => {
            console.error(e);
            return false;
        });

        return contacts;

    } else {
        console.warn('Contact -> get() bez logowania');
        return false;
    }
};

const getOne = async (req, res, next) => {

    let contact;

    if (req.user) {

        await Contact.findOne({ _id: req.params.contactId }).then(result => {
            if (result.ownerId == req.user._id) {
                contact = result;
            } else {
                console.warn('Brak uprawnień');
                return false;
            }
        }).catch(e => {
            console.error(e);
            return false;
        });

        return contact;

    } else {

        console.warn('ontact -> getOne() bez logowania');
        return false;

    }

};


module.exports = {
    add : add,
    update : update,
    deleteOne : deleteOne,
    get : get,
    getOne : getOne
};