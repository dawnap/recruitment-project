module.exports = {
    mongo: {
        db: 'mongodb://localhost/test',
        options: {
            useMongoClient: true
        }
    },
    server: {
        port: '3000'
    }
};