$('.ui.accordion')
  .accordion()
;

$('.ui.dropdown')
  .dropdown()
;

$('.noclickparent')
    .click(function (e) { e.stopPropagation(); })
;

$('.message .close')
  .on('click', function() {
    $(this)
      .closest('.message')
      .transition('fade')
    ;
  })
;